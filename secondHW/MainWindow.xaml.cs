﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace secondHW
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        

        private void RunAwayButton_Click(object sender, RoutedEventArgs e)
        {            
            Random random = new Random();
            double randomNumberX = random.Next(0, 431);
            double randomNumberY = random.Next(0, 702);
            if (runAwayButton.IsMouseOver == true)
            {
                runAwayButton.Margin = new Thickness(randomNumberY, randomNumberX, randomNumberX, randomNumberY);
            }
            else
                MessageBox.Show("Something is wrong!");
        }
    }
}
